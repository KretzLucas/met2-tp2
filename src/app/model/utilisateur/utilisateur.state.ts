import { AddUtilisateur } from './utilisateur.action.add'
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { UtilisateurStateModel } from './utilisateur.state.model';

@State<UtilisateurStateModel>({
    name: 'utilisateur',
    defaults: {
        utilisateur: []
    }
})

export class UtilisateurState {
    @Selector()
        static GetUtilisateur(state: UtilisateurStateModel) {
            return state.utilisateur;
        }

    @Action(AddUtilisateur)
        Add({ getState, patchState }: StateContext<UtilisateurStateModel>, { payload }: AddUtilisateur) {
            const state = getState();
            patchState({
                utilisateur: [...state.utilisateur, payload]
            });
        }
}