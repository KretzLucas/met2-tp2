export class Utilisateur {
    nom: string;
    prenom: string;
    civilite: string;
    adresse: string;
    ville: string;
    codePostal: string;
    email: string;
    telephone: string;
    nomDeCompte: string;
    motDePasse: string;
    confirmation: string;

    constructor() {
    }
}
