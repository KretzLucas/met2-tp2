export class Album {
    id: number;
    titre: string;
    artiste: string;
    annee: number;
    genre: string;
    piste: number;
    image: string;

    constructor() {
    }
}
