import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';
import { Album } from './model/album/album';

@Injectable({
  providedIn: 'root'
})
export class EchangeService {
      
    constructor(private http: HttpClient) { }

    public GetAlbum(): Observable<Album[]> {
        return this.http.get<Album[]>(environment.backendAlbum);
    }
}

