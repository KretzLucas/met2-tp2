import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { PanierRoutingModule } from './panier-routing.module';
import { PanierComponent } from './panier.component';

@NgModule({
  declarations: [PanierComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PanierRoutingModule
  ]
})
export class PanierModule { }
