import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { NgxsModule } from '@ngxs/store';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { UtilisateurState } from './model/utilisateur/utilisateur.state'
import { AlbumState } from './model/album/album.state';

const appRoutes: Routes =[
  {path:'', loadChildren: () => import('./recherche/recherche.module').then(m=> m.RechercheModule)},
  {path:'Acceuil', loadChildren: () => import('./recherche/recherche.module').then(m=> m.RechercheModule)},
  {path:'Catalogue', loadChildren: () => import('./recherche/recherche.module').then(m=> m.RechercheModule)},
  {path:'Panier', loadChildren: () => import('./panier/panier.module').then(m=> m.PanierModule)},
  {path:'Inscription', loadChildren: () => import('./formulaire/formulaire.module').then(m=> m.FormulaireModule)},
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
  ],
  imports: [
      BrowserModule,
      FormsModule,
      ReactiveFormsModule,
      HttpClientModule,
      RouterModule.forRoot(appRoutes),
      NgxsModule.forRoot([
        UtilisateurState,
        AlbumState
      ])
    ],
   bootstrap: [AppComponent]
})
export class AppModule { }
