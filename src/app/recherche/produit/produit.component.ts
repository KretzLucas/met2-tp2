import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngxs/store'
import { EchangeService } from '../../echange.service';
import { Album } from '../../model/album/album';
import { Observable } from 'rxjs';
import { AddAlbum } from '../../model/album/album.action.add'

@Component({
  selector: 'app-produit',
  templateUrl: './produit.component.html',
  styleUrls: ['./produit.component.sass']
})
export class ProduitComponent implements OnInit {
    @Input() barreRecherche: string;
    albums: Observable<Album[]>;

    constructor(private echangeService: EchangeService, private store: Store) { }

    ngOnInit() {
        this.albums = this.echangeService.GetAlbum();
    }

    AjouterAuPanier(album: Album) {
      this.AddAlbum(album);
    }

    AddAlbum(album) { 
      this.store.dispatch(new AddAlbum(album)); 
    }
}
