import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtreAlbum'
})
export class FiltreAlbumPipe implements PipeTransform {

    transform(items: any[], searchText: string): any {
        if (!items) {
            return [];
        }
        if (!searchText) {
            return items;
        }

        searchText = searchText.toLowerCase();
        return items.filter(it => {
            return it.titre.toLowerCase().includes(searchText)
                || it.artiste.toLowerCase().includes(searchText)
                || it.annee.toLowerCase().includes(searchText);
        });
    }
}
